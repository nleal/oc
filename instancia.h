#ifndef INSTANCIA_H_
#define INSTANCIA_H_
#include <iostream>
#include <stdio.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <vector>
#include "linea.h"
using namespace std;

class linea;

class instancia{
	
	private:
		int id_inst;   //Identificador unico de la instancia
		string nombre; //Nombre de la instancia
		linea *linea_p; //Apuntador que dice a que linea pertenece la instancia
		int    num_vinculado;	//Numero de lineas con las que el producto academico esta vinculado
		vector <linea*> vinculado_con; //Vector de lineas con las cual esta vinculado el producto academico
	
	public:	
		//Constructor
		instancia(); 
		instancia(int id_int, string nomb, linea * l);
		
		//Destructor
		~instancia();
		
		//Observadores
		string get_nombre_ins();
		linea* get_linea_p();
		linea* get_vinculado_con(int n);
		int get_num_vinculado();
		int get_id_instancia();
		
		//Modificadores
		bool agregar_linea_p(linea *l);
		bool agregar_vinculo(linea *vinc);


};
	//Constructor basico
	instancia::instancia(){
		
		
		}
	//Constructo con parametros	
	instancia:: instancia(int id_int , string nomb, linea * l){
			id_inst = id_int;
			nombre = nomb;
			linea_p = l;
			num_vinculado = 0;
		}
		
	//Destrutor
	instancia::~instancia(){
		}	
		
	//Consulta el nombre de la instancia	
	string instancia:: get_nombre_ins(){
			return nombre;		
		} 
		
	//Consulta la linea a la que pertence la instancia	
	linea* instancia:: get_linea_p(){
		return linea_p;
		}
    
    //Consulta en una posicion dada del vector , con quien esta vinculada la instancia		
	linea* instancia:: get_vinculado_con(int n){
			return vinculado_con[n];		
		}
	//Consulta el numero de vinculacion que tiene la instancia 	
	int instancia:: get_num_vinculado(){
			return num_vinculado;
		}	
		
    //Consulta el identificador unico de la instancia		
	int instancia:: get_id_instancia(){
			return num_vinculado;
		}	
	
	//Modificadores 
		
	bool instancia:: agregar_linea_p(linea *l){
			linea_p = l;
		}
	
	bool instancia:: agregar_vinculo(linea *vinc){
			vinculado_con.push_back(vinc);
			num_vinculado+=1;
		}		
#endif
