#include "oc.h"
#include <fstream>
#include <string.h>
#include <map>
#include <fstream>
#include <limits.h>
#include <float.h>
#include <cstdlib>
#include <list>
#include <vector>
#include <sstream>
#include <iostream>
#include <pqxx/pqxx> 
#include <sys/types.h>
#include <regex.h>

using namespace std;
using namespace pqxx;

int main()
{

	oc  *oc_b = new oc();
	
	int num_lin ,f, tam, num_insta;
	num_lin = 150;
	num_insta = 415;
	
	bool es_linea = false;
	bool existe_linea = false;
	bool es_sub_linea = false;
	bool se_creo_ins = false;
	string q, consulta, qq, var11 , sql, str, p;
	vector <int> w;
	string consulta1;
	string cons, cons1, final1, final2;
	string consul_final,sql_principal, sql_linea, sql_f ,sql2;
	string sql7, sql4 , s1, x, s2;
	int len, num, num2, res, i = 0;
	string resulto,src, var1, name, seguir= "s";
   
	string pattern = "'.*?'";  //Patron de la expresion regular 
	regex_t preg;
	vector <string> clausula;
	string clausulaf;
	regmatch_t pmatch;
  
   
   try{
      connection C("dbname=oc user=com password=12345 hostaddr=127.0.0.1 port=5432"); //Conexion a base de datos  con parametros
      
      if (C.is_open()) {
         cout << "Opened database successfully: " << C.dbname() << endl;   //Si todos los datos son correctos se conecta al modelo de base de datos requerido
      } else {
         cout << "Can't open database" << endl;  //Si algun parametro esta erroneo no se conecta a la base de datos
         return 1;
      }
            
	  nontransaction N(C);  // Se crea el objeto  non-transactional  con respecto a la conexion 
      
      
     result R( N.exec("SELECT * from linea_inv")); // se ejecuta Query SQL para tomar los datos de las lineas de investigacion 
      
      
     for (result::const_iterator c = R.begin(); c != R.end(); ++c) {   
        oc_b->crear_linea(c[0].as<int>() , c[2].as<string>());			//Se crean todas las lineas tomadas de la base de datos
		oc_b->agregar_sub_linea_id(c[0].as<int>(), c[3].as<int>());		//Se agregan sub lineas de las lineas creadas 
      }
      
      //Se ejecuta consulta sql para importar instancias 
      result P( N.exec("select p.id_productos, p.nombre_producto, p.autor, l.nombre_linea from  producto p join linea_inv l on l.id_linea= p.id_linea"));
      
      
      for (result::const_iterator c = P.begin(); c != P.end(); ++c) {
      	     oc_b->crear_instancia(c[0].as<int>(),c[3].as<string>(),c[1].as<string>()); // Se crean las instancias en la ontologia 
      }
      
     
     //Se ejecuta Query Sql para importar los datos de las instancias vinculadas con las lineas 
      result O( N.exec("select p.nombre_producto, l.nombre_linea from  vinculado_con v join linea_inv l on l.id_linea= v.id_linea join producto p on v.id_producto = p.id_productos"));
     
	  for (result::const_iterator c = O.begin(); c != O.end(); ++c) {
         oc_b->vinculado_con(c[0].as<string>(),c[1].as<string>());   //Se crean las vinculaciones 
      }

   while(seguir=="s"){
   cout<<"Ingrese su consulta"<<endl;
   std::getline (std::cin,src);
   
    num2 = src.find("and",1);
    sql_principal= src.substr(0,num2+3);
    
    cout<<"consulta principal  "<<sql_principal<<endl;
    cout<<"---------------------------------------------"<<endl;
    
   regcomp(&preg, &pattern[0], REG_EXTENDED);   //Se verifica la expresion regular 
 
   do{
       resulto = regexec(&preg, &src[0], 1, &pmatch, 0);
       if(res == REG_NOMATCH)
       {
          cout << "NO match\n" << endl;
          num = -1;
       }
       else{ // Si el patron esta bien 
           var1 = src.substr( (int)pmatch.rm_so, (int)pmatch.rm_eo - 3);
          
           var1 = src.substr( (int)pmatch.rm_so, (int)pmatch.rm_eo);
           num = var1.find("'",1);
           src = var1.substr(num + 1);
          // cout << "coincidencia: " << var1.substr(1, num-1) << endl;
           clausula.push_back( var1.substr(1, num-1));
           clausulaf=( var1.substr(1, num-1));
       }
    }while( src.length() != 0 && src.length() != 1 && src.length() !=2 && num != -1);
    
   regfree(&preg);
   
   
   cout<<"la linea a buscar  "<<endl;
	oc_b->existe_linea("Linea2");
	cout<<"----->aqui<-------------------- "<<endl;
	
    cout<<"primera busqueda "<<endl;
    
    oc_b->existe_linea("Linea2");
    oc_b->busqueda(clausulaf);   // Busca en la ontologia todos los productos que pertenecen o estan vinculados a la Linea que se le envia como parametro 
    cout<<"segunda busqueda "<<endl;
    w = oc_b->busqueda_where(clausulaf); //Busca todos los identificadores que seran agregados para reescribir la consulta final, que sera ejecutada en PostgreSQL
    
    
    tam= w.size();
    
    for(int i=0;i<tam;i++){
			stringstream ss1;
			ss1<< w[i];
			if(i==tam-1){

				cons1 =  cons1 +ss1.str();
				
			}else{
				cons1 =  cons1 +ss1.str()+",";
				
				}
	}
	
	sql_linea = " l.id_linea in (" + cons1 + ")";
	
	//cout<< "Final de linea " << sql_principal+sql_linea<<endl;
	
	sql2 = " union select p.id_productos ,  p.nombre_producto from    vinculado_con v join producto p on p.id_productos=v.id_producto where v.id_linea in (" + cons1+ ")";

	sql_f = sql_principal+sql_linea+sql2;		
    
    cout<< " "<< endl;
    cout<< "SQL  Re-Escrita : " <<sql_f<< endl;
    cout<< " "<< endl;
    string s;
    s = sql_f;

      result G( N.exec(s));

      for (result::const_iterator c = G.begin(); c != G.end(); ++c) {
         cout << "ID = " << c[0].as<int>();
         cout << "   Name = " << c[1].as<string>() << endl;
       
      }

    cout<< " "<< endl;
    cout<< "SQL  Re-Escrita : " <<sql_f<< endl;
    cout<< " "<< endl;
    
    cout<<"Desea hacer otra consulta ?"<<endl;
    cin>>seguir;

}

      /**strlen   numero de caracteres de un string**/
      cout << "Operation done successfully" << endl;      
      
      C.disconnect ();
   }catch (const std::exception &e){
      cerr << e.what() << std::endl;
      return 1;
   }
   
   // SELECT id_productos, nombre_producto   FROM producto p, linea_inv l  WHERE  p.id_linea=l.id_linea   and l.nombre_linea = 'Linea
      
}
