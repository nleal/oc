#include "oc.h"
#include <fstream>
#include <string.h>
#include <map>
#include <fstream>
#include <limits.h>
#include <float.h>
#include <cstdlib>
#include <list>
#include <sstream>
using namespace std;

using std::set_new_handler;
#include <cstdlib>
using std::abort;

void mensaje_error(){
	cerr << "Error de memoria, bad_alloc";
	abort();
}


int main(){
	
	
	set_new_handler( mensaje_error ); //Llama al metodo cuando hay una sobrecarga de memoria, mensaje personalizado
	
	
	
	oc  *oc_b = new oc();
	string l, l1, l2, lh, lp, li, l_ins,lv, iv, aux;
	int i, j, k, f;
	int num_lin ;
	int num_insta ;
	bool es_linea = false, se_creo, agrego_sub, agrego_sub2;
	bool se_creo_ins = false;
	bool son_familia = false;
	
	
	//num_lin = rand()%30 + 270;
	//num_insta = rand()%30 + 300;
	
	
	num_lin = 300;
	num_insta = 830;
	
	
	
	
	/*Se van a generar  lineas de investigacion*/
	
	cout<<"Se crearan :   "<<num_lin << "  "<<"Lineas de investigacion.  " <<endl;
	
	for(i=1; i<= num_lin; i++){
			stringstream ss;
			ss << i;
			l = "Linea" +ss.str();
			se_creo = oc_b->crear_linea(i,l);
			
			if(se_creo==true ){
				//cout<< "Se creo la linea " <<l << endl;
					
			}
	}
		
	/*Agregando sub_lineas*/

		int num_i ;
		num_i = ((num_lin /2 )-1);
		for(j=1;j<=num_i;j++){
			stringstream s1, s2, s3;
			s1<< j;
			s2<<(j*2);
			s3<<(j*2)+1;
			
			l  = "Linea" +s1.str();
			l1 = "Linea" +s2.str();
			l2 = "Linea" +s3.str();
			
			agrego_sub = oc_b->agregar_sub_linea(l1, l);
			agrego_sub = oc_b->agregar_sub_linea(l2, l);
		
		}
		
		
	/*Verificar si una linea es sub linea de otra*/
		/***
		for(k=1;k<=num_lin;k++){
			stringstream ss;
			ss << k;
			lp = "Linea" +ss.str();
			for(f=1;f<=num_lin;f++){
				stringstream ss1;	
				ss1<< f;
				lh = "Linea" +ss1.str();
				es_linea = oc_b->sub_linea(lh, lp);
				if(es_linea== true){
					cout << "la linea --->" << lh << "Es sub linea de : "  <<  lp <<endl;
					}
			}						
		}**/
		
		
	/*Crear Instancias */
		
		cout<<"Se crearan   "<<num_insta << "  "<<"Instancias.  " <<endl;
		
	for(i=1; i<= num_lin; i++){
			stringstream sl1;
			sl1 << i;
			li = "Linea" +sl1.str();
			
		for(j=1;j<=num_insta;j++){
			
			stringstream si1;
			si1 << j;
			l_ins = "Instancia" +si1.str() + "-" + li;	
			
			se_creo_ins = oc_b->crear_instancia(j,li,l_ins);
			
			if(se_creo_ins==true ){
				//cout<< "Se creo la instacia  " <<l_ins <<"En la linea " << li<<endl;
					
			}
		}	
			
	} 
	
	/*Crear vinculaciones */

		/*vinculacion de instancias con otras lineas */
		/**veo que la linea a vincular no sea familia */

int x;

	/**lo vinculare con nlineas*/
	
	for(i=1; i<= num_lin; i++){ 
			stringstream sl1;
			sl1 << i;
			lv = "Linea" +sl1.str();
		for(j=1;j<=num_lin;j++){
			
			stringstream slv;
			//x = rand()%30 + 50;
			x=j;
			slv << x ;
			
			aux = "Linea" +slv.str();
			stringstream si1;
			si1 << j;
			iv = "Instancia" +si1.str() + "-" + aux;	
			
			son_familia = oc_b->familia(iv, lv );  //si es familia no se puede vincular 
			//cout<<"vinculamos   : "<<lv<< "  con     :"<<iv<<endl;
			if(son_familia==true){
				//cout <<"nooooo   podemos vincular"<< endl;
			}else{
					oc_b->vinculado_con(iv,lv);
					
			}
			
		}		
		
	}
	  /*oc_b->todos_los_hijos("Linea1");
      oc_b->todos_los_padres("Linea1");
	
	
	oc_b->vinculado_con("Instancia150-Linea150","Linea4");
	oc_b->vinculado_con("Instancia75-Linea75","Linea4");
	oc_b->vinculado_con("Instancia45-Linea45","Linea4");
	oc_b->vinculado_con("Instancia25-Linea25","Linea4");
	oc_b->vinculado_con("Instancia15-Linea15","Linea4");
	oc_b->vinculado_con("Instancia13-Linea13","Linea4");
	
	
	
	
	/*oc_b->vinculado_con("Instancia150-Linea150","Linea16");
	oc_b->vinculado_con("Instancia75-Linea75","Linea16");
	oc_b->vinculado_con("Instancia45-Linea45","Linea16");
	oc_b->vinculado_con("Instancia25-Linea25","Linea16");
	oc_b->vinculado_con("Instancia15-Linea15","Linea16");
	oc_b->vinculado_con("Instancia13-Linea13","Linea16");
	oc_b->vinculado_con("Instancia150-Linea150","Linea16");
	oc_b->vinculado_con("Instancia75-Linea75","Linea16");
	oc_b->vinculado_con("Instancia45-Linea45","Linea16");
	oc_b->vinculado_con("Instancia25-Linea25","Linea16");
	oc_b->vinculado_con("Instancia15-Linea15","Linea16");
	oc_b->vinculado_con("Instancia13-Linea13","Linea16");
	
	
	
	/*
	oc_b->vinculado_con("Instancia150-Linea150","Linea56");
	oc_b->vinculado_con("Instancia75-Linea75","Linea56");
	oc_b->vinculado_con("Instancia45-Linea45","Linea56");
	oc_b->vinculado_con("Instancia25-Linea25","Linea56");
	oc_b->vinculado_con("Instancia15-Linea15","Linea56");
	oc_b->vinculado_con("Instancia13-Linea13","Linea56");
	oc_b->vinculado_con("Instancia300-Linea8","Linea56");
	oc_b->vinculado_con("Instancia150-Linea16","Linea56");
	oc_b->vinculado_con("Instancia75-Linea5","Linea56");
	oc_b->vinculado_con("Instancia45-Linea10","Linea56");
	oc_b->vinculado_con("Instancia25-Linea20","Linea56");
	oc_b->vinculado_con("Instancia15-Linea40","Linea56");
	oc_b->vinculado_con("Instancia13-Linea72","Linea56");
	oc_b->vinculado_con("Instancia300-Linea144","Linea56");
	oc_b->vinculado_con("Instancia75-Linea64","Linea56");
	oc_b->vinculado_con("Instancia45-Linea36","Linea56");
	oc_b->vinculado_con("Instancia25-Linea143","Linea56");
	oc_b->vinculado_con("Instancia13-Linea73","Linea56");
	oc_b->vinculado_con("Instancia150-Linea37","Linea56");
	oc_b->vinculado_con("Instancia75-Linea19","Linea56");
	oc_b->vinculado_con("Instancia45-Linea10","Linea56");
	oc_b->vinculado_con("Instancia25-Linea18","Linea56");
	oc_b->vinculado_con("Instancia15-Linea31","Linea56");
	oc_b->vinculado_con("Instancia13-Linea5","Linea56");
	*/
	
	
	
	
	/*oc_b->busqueda("Linea4");
	//oc_b->busqueda("Linea16");
	//oc_b->busqueda("Linea56");
	*/
	
	 
}
