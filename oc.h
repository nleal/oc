#ifndef  OC_H_
#define OC_H_
#include <vector>
#include "instancia.h"

using namespace std;

class oc{
		private:
			int num_lineas;  //Numero de lineas que tiene la ontologia
			int num_instancias; //Nuemero de instancias que tiene la ontologia
			vector <linea*> lineas; //Vector que lleva las lineas existentes
			vector <instancia*> instancias; //Vector que lleva las instancias existentes
			linea *lin;  //Indice de las lineas 
			
		public:
		//Constructores
			oc(); 
			oc(int id,string nom);
		//Destructor	
			~oc();
			
			//Crear 
			bool crear_linea(int id, string nom_linea);
			bool agregar_sub_linea(string linea_h, string linea_p);
			bool agregar_sub_linea_id(int linea_h, int linea_p);
			bool crear_instancia(int id_inst, string linea_i, string instanci);
			
			//Consultas
			linea* get_linea(string nomb);
			linea* get_linea(int id);
			instancia* get_instancia(int pos);
			instancia* get_instancia(string in);
			bool existe_linea(string nomb);
			bool existe_id_linea(int id);
			void existe_instancias_linea(string linea);
			int get_num_lineas();
			bool sub_linea(string nom_linea, string linea_padre);
			bool vinculado_con(string inst, string lin);
			bool busqueda(string line);
			bool familia(string ins, string line);
			void todos_los_padres(string lin);
			void todos_los_hijos(string lin);
			void ins_vinculado_linea(string ins);
			vector <int> busqueda_where(string line);
};
	//Constructor 
	oc:: oc(){
		num_lineas = 0;
		num_instancias = 0;
		lin = NULL;
		}
		
	//Constructor con parametros	
	oc:: oc(int id, string nom){
		
		lin = new linea(id,nom);
		}	
		
	//Desructor
	oc::~oc(){
		
		}
	//Consultar si una linea existe en la ontologia 
	bool oc:: existe_linea(string nomb){
		int i , tam_li= 0;
		bool esta = false;
		tam_li = lineas.size();
		
			for (i=0; i<tam_li;i++){
				if (nomb == lineas[i]->get_nombre()){	
					esta = true ;
					cout<<"si existe"<<endl;
				}else {
					cout<<"NO existe"<<endl;
					
					}
			}
			return esta;
	}
	//Consulta si el id de una linea existe 
	bool oc:: existe_id_linea(int id){
		int i , tam_li= 0;
		bool esta = false;
		tam_li = lineas.size();
			for (i=0; i<tam_li;i++){
				if (id == lineas[i]->get_id_linea()){	
					esta = true ;
				}
			}
			return esta;
	}
	
	//Obtener el apuntador donde se ubica una linea dado su nombre 
	linea* oc::get_linea(string nomb){
		linea  *p = NULL;
		int i, tam_l =0;
		tam_l = lineas.size();
		for(i=0;i<tam_l;i++){
			if(nomb == lineas[i]->get_nombre()){
				p = lineas[i];
			}
		}
		return p;
	}
	
	//Obtener el apuntador donde se ubica una linea dado su id 
	linea* oc::get_linea(int id){
		linea  *p = NULL;
		int i, tam_l =0;
		tam_l = lineas.size();
		for(i=0;i<tam_l;i++){
			if(id == lineas[i]->get_id_linea()){
				p = lineas[i];
			}
		}
		return p;
	}
	
	//Crea una linea en la ontologia
	bool  oc::crear_linea(int id, string nomb){
		
		bool creada = false;
		if(existe_id_linea(id)  && existe_linea(nomb)){
			cout<<"La linea de insvestigacion ya existe"<<endl;
		}else{
				linea *l = new linea(id,nomb);
				lineas.push_back(l);		
			}
	}
	
	//Se crea la relacion SubLinea  linea_h sera sublinea de liena_p
	//Esta funcion se usa para los casos genericos de prueba
	bool oc::agregar_sub_linea(string linea_h, string linea_p){
		
		bool creada_sub = false;
		linea *hijo  = get_linea(linea_h);
		linea *padre = get_linea(linea_p);
		int i, num_p =	hijo->get_num_padres();  // Sabemos el numero de lineas de las cual es padre la variable "Hijo"
		int num_h =  padre->get_num_hijos();
		
		
		if(hijo!=NULL && padre!=NULL){    //Verificar que las lineas existan
			padre->agregar_padre(hijo );   //Se agrega al "Vector Padre" el apuntador hacia "Linea_h" 
					
			if(num_p>0){
				for(i=0;i<num_p;i++){  
					linea *padre_t = get_linea(hijo->get_padre(i)->get_nombre());
					padre->agregar_padre(padre_t);  //Todos los sucesores que tiene "Linea_h"  se agregan como sucesores a "Linea_p"
					padre_t->agregar_hijos(padre);  //Todos los sucesores que  tiene "Linea_h" se agregan acomo sucesores a "Linea_p"
					
				}
			}
			hijo->agregar_hijos(padre); // Se agrega al  "Vector hijo" el apuntador hacia "Linea_p"
		
			if(num_h>0){
				for(i=0;i<num_h;i++){
					linea *hijo_t = get_linea(padre->get_hijos(i)->get_nombre());
					hijo->agregar_hijos(hijo_t);  // Todos los ancestros de "Linea_p"  se agregan al "Vector Hijos" de "Linea_h"
					hijo_t->agregar_padre(hijo);  //Cada ancestro de "Linea_p" ahora sera tambien padre de "Linea_h"
				}
			}
			creada_sub= true;
	   }
		return creada_sub;
	}
	
	
	
	//Agregar sub linea recibiendo los id de las lineas 
	//Esta funcion se usa para los casos donde los datos son importados de la base de datos
	bool oc::agregar_sub_linea_id(int linea_h, int linea_p){
	bool creada_sub = false;

		linea *hijo  = get_linea(linea_h);
		linea *padre = get_linea(linea_p);
		
		if(hijo!=NULL && padre!=NULL){    
			padre->agregar_padre(hijo );   
		
			int num_p =	hijo->get_num_padres();  
			int i ;
				
			if(num_p>0){
					for(i=0;i<num_p;i++){
						linea *padre_t = get_linea(hijo->get_padre(i)->get_nombre());
						padre->agregar_padre(padre_t);
					padre_t->agregar_hijos(padre);
					
				}
			}
			hijo->agregar_hijos(padre);	
			int num_h =  padre->get_num_hijos();
			if(num_h>0){
				for(i=0;i<num_h;i++){
					linea *hijo_t = get_linea(padre->get_hijos(i)->get_nombre());
					hijo->agregar_hijos(hijo_t);
					hijo_t->agregar_padre(hijo);
				}
			}
	
			return true;
		}else{
			
			return false;
		}
	}
	
	// Determinar si Nom_Linea es sub linea de  Linea_padre
	bool oc:: sub_linea(string nom_linea, string linea_padre){  
		int id;
		bool es_sub = false;
		linea *lin  = get_linea(nom_linea);
		linea *padre = get_linea(linea_padre);
		id = lin->get_num_padres();
			
		if(lin!=NULL && padre!=NULL && nom_linea!=linea_padre){
			
			int n_padres = padre->get_num_padres();
			int n_h = padre->get_num_hijos();
			int j =0;
			while (j<n_padres && !es_sub){
				if(padre->get_padre(j)->get_nombre()== nom_linea){
						es_sub = true;		
				}
				j=j+1;
			}
		return es_sub;
		}
	}
	
	//Consultar la posicion de la instancia 
	instancia* oc::get_instancia(int pos){
				return instancias[pos];	
	}
	
	//Obtener la posicion de una instancia 
	instancia* oc::get_instancia(string inst){
		instancia  *n = NULL;
		int i;
		for(i=0;i<num_instancias;i++){
			if(inst==instancias[i]->get_nombre_ins()){
				n = instancias[i];
			}
		}
		
		return n;
	}
	
	//Se crea una instancia 
	bool oc::crear_instancia(int  id_inst, string linea_i, string instanci){
		bool bien = false;
		linea *aux = get_linea(linea_i);
		instancia *aux2;
		
		if(aux!= NULL){
				instancia *ins = new instancia(id_inst, instanci, aux);
				instancias.push_back(ins);
				aux2 = get_instancia(num_instancias);
				aux->agregar_instancia(aux2);
				num_instancias+=1;
				bien = true; 
		}
		return bien;
	}
	
	//Consultar todas las instancias que posee una linea 
	void oc::existe_instancias_linea(string l){
		
		linea *linea_buscar  = get_linea(l);
		int i ;
		for(i=0;i<linea_buscar->get_num_instancias();i++){
			instancia *in = linea_buscar->get_instancia(i);
			cout<< in->get_nombre_ins()<<endl;
		}
	}
	
	//Esta funcion vincula una instancia con una determinada linea
	bool oc::vinculado_con(string inst, string lin){
			linea *linea_v = get_linea(lin);
			instancia *instancia_v = get_instancia(inst);
			
			if(linea_v != NULL && instancia_v!= NULL){
				instancia_v->agregar_vinculo(linea_v);		
				linea_v->agregar_trabajo_vinculado(instancia_v);
			}
	}
	
	//Esta funcion busca todos los productos academicos que pertenecen o estan vinculados con la linea dada
	bool oc::busqueda(string line){
			
		vector <instancia*> respuesta;	
		vector <instancia*> v_aux;	
		linea *aux = get_linea(line);
		linea *aux2;
		v_aux = aux->respuestas_iv(respuesta); //Busca los productos academicos que pertenecen o estan vinculados a la linea
		 
		respuesta.size();
		int k,j;
		
		for(k=0;k<aux->get_num_padres();k++){	 
				 aux2 = aux->get_padre(k);
				 v_aux= aux2->respuestas_iv(respuesta);  //para cada sucesor busca y agrega los productos academicos que estan vinculados o pernecen  
		}
		
		for(j=0;j<v_aux.size();j++){
			 cout<<"La respuesta es :" <<v_aux[j]->get_nombre_ins()<<endl;		
			}
		
	}
	
	//Esta funcion se usa para validad si la linea con la que se quiere vincular es sucesor de la linea dada
	bool oc::familia(string ins, string line){
		linea* auxLP=NULL;
		instancia* aux2=NULL;
		bool resp = NULL, resp2 = NULL;
		string linea_p, linea_v;
		
		linea* aux = get_linea(line);  //Apunta a la linea que quiero vincular
		aux2 = get_instancia(ins);	//Apunta a la instancia que voy  a vincular
		auxLP = aux2->get_linea_p();  //Apunta a la linea que pertenece la instancia que voy a vincular 
			
		linea_p = auxLP->get_nombre();
		linea_v = aux->get_nombre();
		
		resp = sub_linea(linea_p, linea_v);
		resp2 = sub_linea(linea_v, linea_p);
		    
		if(linea_p != linea_v)  {  //Si no es la misma linea a la cual ya pertenece
		
			if (resp == false  &&  resp2 == false ){
				return false ;  //No son familia ni son la misma linea, asi que si puede agregarse
			}		
		}else return true;   // Si son familia y NO SE PUEDE 
				
	}
	
	
	//Consulta todas las lineas de la cual es padre , es decir su sucesores
	void oc::todos_los_padres(string lin){
		
		int n, i;
		linea* aux = get_linea(lin);  //apunta a la linea que voy a buscarle los padres
		n = aux->get_num_padres();
		cout<<lin<< " Es padre de  : "<<endl;
		for(i=0;i<n;i++){
			cout<<"-"<<aux->get_padre(i)->get_nombre()<<endl;	
		}
	}
	
	
	//Consulta toaslas lineas de las cual es hija , es decir todos sus ancestros
	void oc::todos_los_hijos(string lin){
		
		int n, i;
		linea* aux = get_linea(lin);  //Apunta a la linea que voy a buscarle los hijos
		n = aux->get_num_hijos();
		cout<<lin<< " Es hija de  "<<endl;
		for(i=0;i<n;i++){
			cout<<"-"<<aux->get_hijos(i)->get_nombre()<<endl;	
		}
	}
	
	//Consultar con que lineas esta vinculada una instancia
	void oc::ins_vinculado_linea(string ins){
		
		instancia *n = get_instancia(ins);
		for(int i=0;i<n->get_num_vinculado();i++){
			cout<< "linea :  "<<n->get_vinculado_con(i)->get_nombre()<<endl;
		}
		
	}
		
	//Esta funcion busca todos los id para reescribir la consulta
	vector <int> oc:: busqueda_where(string lin){   
	    vector <int> respuesta; 
	    int n, i;
		linea* aux = get_linea(lin);  
		n = aux->get_num_padres();
		//todos_los_padres(lin);
		//todos_los_hijos(lin);
		respuesta.push_back(aux->get_id_linea());	
		
		for(i=0;i<n;i++){
			respuesta.push_back(aux->get_padre(i)->get_id_linea());	
		}
	    return respuesta;
	}

#endif
