#ifndef LINEA_H_
#define LINEA_H_
#include <iostream>
#include <list>
#include <fstream>
#include <string.h>
#include <map>
#include <fstream>
#include <limits.h>
#include <float.h>
#include <cstdlib>
#include <list>
#include <vector>
#include <sstream>

using namespace std;

class instancia;

class linea{
		private:
			string nombre; //Nombre de la linea
			int num_padres; //Numero lineas del cual es padre  la linea
			int num_hijos;  //Numero lineas de la cual es hijo 
			int num_instancias; //Numero de productos academicos que pertenecen a la linea
			int num_trab_vin;	//Numero de productos acdemicos vinculados a la linea
			int id_linea;		//Identificador unico en la base de datos 
			vector <linea*> padres; //Vector de todas las lineas de la cual es padre esta linea
			vector <linea*> hijos;  //Vector de todas las lineas de cual es hijo
			vector <instancia*> instancias_p; //Vector de instancias que posee la linea   
			vector <instancia*> trabajos_vinculados; //Vector de productos academicos vinculados a la linea 
		
		public: 
			//Constructor		
			linea();
			linea (int id , string n) ; 
			
			//Destructor
			~linea();
			
			//Observadores 
			string get_nombre();
			int get_id_linea(); 
			int get_num_padres();
			int get_num_hijos();
			int get_num_instancias();
			int get_num_trab_vincu();
			linea* get_padre(int n);
			linea* get_hijos(int n);
			instancia* get_instancia(int n);
			vector <instancia*>  respuestas_iv(vector <instancia*> &respuesta1);
			vector <instancia*>  copiar_instancias(vector <instancia*> copia);
			vector <instancia*>  copiar_trabajos_vincu(vector <instancia*> copia);
			
			//Modificadores   
			void set_nombre(string nom); 
			void agregar_padre(linea *padre);
			void agregar_hijos(linea *hijo);
			void agregar_instancia(instancia *inst);
			void agregar_trabajo_vinculado(instancia *inst); //Se usa porque el arco es bidireccional y asi saber quien se vincula con la linea 
			
			
};
	//Constructor vacio
	linea::linea(){
		
		}

	//Constructor con parametros
	linea::linea(int id, string n){
			nombre = n;
			id_linea = id;
			num_padres = 0;
			num_hijos = 0;
			num_instancias = 0; 
			num_trab_vin = 0; 
	}
	
	//Destructor
	linea::~linea(){
		
		}
	//Consultar el nombre de la linea
	string linea:: get_nombre(){
			return nombre;
		}
	//Consulta la linea
	int linea:: get_id_linea(){
			return id_linea;
		}
	//Consultar el numero de lineas de la cual es padre
	int linea:: get_num_padres(){
			return num_padres;
		}
	//Consultar el numero de lineas de la cual es hijo
	int linea:: get_num_hijos(){
			return num_hijos;		
		}
	//Consulta el numero de productos academicos  	
	int linea:: get_num_instancias(){
		
			return num_instancias;
		}
	//Consulta el numero de productos academicos vinculados  que pose las linea	
	int linea:: get_num_trab_vincu(){
			return num_trab_vin;
		}
	//Consulta en una posicion dada una linea  de la cual es padre		
	linea* linea:: get_padre(int n){
			return padres[n];
		} 	
	//Consulta en una posicion dada una linea de la cual es hijo	
	linea* linea:: get_hijos(int n)	{
			return hijos[n];
		}
	//Consulta en una posicion dada un producto academico
	instancia* linea:: get_instancia(int n)	{
			return instancias_p[n];
		}		
		
	//Modifica el nombre de la linea	
	void linea:: set_nombre(string nom)	{
			nombre = nom;
		}
	
	//Agrega al vector de padres una linea, la cual  es su hijo  	
	void linea::agregar_padre(linea *padre){
			padres.push_back(padre);
			num_padres+=1;
		}
		
	//Agrega al vector de hijos una linea , la cual es su padre  		
	void linea::agregar_hijos(linea *hijo){
			hijos.push_back(hijo);
			num_hijos+=1;
		}
	//Agrega un producto academico a una linea 	
	void linea::agregar_instancia(instancia *inst){
			instancias_p.push_back(inst);
			num_instancias+=1;
					
		}		
	//Agrega un producto academico el cual esta vinculado	
	void linea::agregar_trabajo_vinculado(instancia *inst){
			trabajos_vinculados.push_back(inst);
			num_trab_vin+=1;
					
		}
		
		
	vector <instancia*> linea::copiar_instancias(vector <instancia*> respuesta1){
		int i, num_tam = 0;
		string  b;
		instancia *ins = NULL;
		num_tam = instancias_p.size();
		for(i=0;i<num_tam;i++){
			 ins = instancias_p[i];
			respuesta1.push_back(ins);
			}
		 return respuesta1;
		}	
		
	vector <instancia*>  linea::copiar_trabajos_vincu(vector <instancia*> copia){
		int i, num_trab = 0;
		instancia *ins = NULL;
		num_trab = trabajos_vinculados.size();
		for(i=0;i<num_trab;i++){
			 ins = trabajos_vinculados[i];
			copia.push_back(ins);
		}
		
		return copia;
		}	
		
	
	vector <instancia*> linea::respuestas_iv(vector <instancia*> &respuesta1){
		
		int i, num_ins = 0;
		string  b;
		instancia *ins = NULL;
		num_ins = instancias_p.size();
		for(i=0;i<num_ins;i++){
			 ins = instancias_p[i];
			respuesta1.push_back(ins);
			}
			
		int k , num_trb=0;
		instancia *ins1 = NULL;
		num_trb = trabajos_vinculados.size();
		for(i=0;i<num_trb;i++){
			 ins1 = trabajos_vinculados[i];
			respuesta1.push_back(ins1);
		}
	 return respuesta1;
		
	}	
		
				
#endif
